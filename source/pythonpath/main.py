#!/usr/bin/env python3

import dialogs
import controllers


ID_EXTENSION = ''


def make_pdf():
    dialog = dialogs.create_make_pdf(ID_EXTENSION)
    dialog.events = controllers.MakePdf(dialog)
    dialog.open()
    return


def report():
    dialog = dialogs.create_report(ID_EXTENSION)
    dialog.events = controllers.Report(dialog)
    dialog.open()
    return


def run(args):
    globals()[args]()
    return
