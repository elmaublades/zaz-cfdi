#!/usr/bin/env python3

import easymacro as app


APP = 'zaz-cfdi'
BUTTON_IMAGE = 20
BUTTON_LABEL = 65


def create_make_pdf(id_extension):
    args= {
        'Name': 'dlg_make_pdf',
        'Title': 'Generar PDF',
        'Width': 200,
        'Height': 100,
    }
    dialog = app.create_dialog(args)
    dialog.id_extension = id_extension

    args = {
        'Type': 'Label',
        'Name': 'lbl_source',
        'Label': 'Selecciona la carpeta origen',
        'Width': 150,
        'Height': 15,
        'Border': 1,
        'Align': 1,
        'VerticalAlign': 1,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Text',
        'Name': 'txt_source',
        'Width': 170,
        'Height': 12,
        'Border': 0,
        'ReadOnly': True,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Button',
        'Name': 'cmd_source',
        'Width': BUTTON_IMAGE,
        'Height': BUTTON_IMAGE,
        'ImageURL': 'source.png',
        'FocusOnClick': False,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Button',
        'Name': 'cmd_pdf',
        'Label': 'Generar',
        'Width': BUTTON_LABEL,
        'Height': BUTTON_IMAGE,
        'ImageURL': 'pdf.png',
        'FocusOnClick': False,
        'ImagePosition': 1,
    }
    dialog.add_control(args)

    dialog.center(dialog.lbl_source, y=5)
    dialog.txt_source.move(dialog.lbl_source, y=8)
    dialog.cmd_source.move(dialog.lbl_source)
    dialog.cmd_pdf.move(dialog.cmd_source)
    dialog.cmd_pdf.center()

    controls = (dialog.txt_source, dialog.cmd_source)
    dialog.center(controls)

    dialog.txt_source.value = app.get_config('pdf', '', APP)

    return dialog


def create_report(id_extension):
    args= {
        'Name': 'dlg_report',
        'Title': 'Generar Reporte',
        'Width': 200,
        'Height': 120,
    }
    dialog = app.create_dialog(args)
    dialog.id_extension = id_extension

    args = {
        'Type': 'Label',
        'Name': 'lbl_source_dir',
        'Label': 'Selecciona la carpeta origen',
        'Width': 150,
        'Height': 15,
        'Border': 1,
        'Align': 1,
        'VerticalAlign': 1,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Text',
        'Name': 'txt_source_dir',
        'Width': 170,
        'Height': 12,
        'Border': 0,
        'ReadOnly': True,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Button',
        'Name': 'cmd_source_dir',
        'Width': BUTTON_IMAGE,
        'Height': BUTTON_IMAGE,
        'ImageURL': 'source.png',
        'FocusOnClick': False,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Label',
        'Name': 'lbl_source_template',
        'Label': 'Selecciona la plantilla del reporte',
        'Width': 150,
        'Height': 15,
        'Border': 1,
        'Align': 1,
        'VerticalAlign': 1,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Text',
        'Name': 'txt_source_template',
        'Width': 170,
        'Height': 12,
        'Border': 0,
        'ReadOnly': True,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Button',
        'Name': 'cmd_source_template',
        'Width': BUTTON_IMAGE,
        'Height': BUTTON_IMAGE,
        'ImageURL': 'ods.png',
        'FocusOnClick': False,
    }
    dialog.add_control(args)

    args = {
        'Type': 'Button',
        'Name': 'cmd_report',
        'Label': 'Reporte',
        'Width': BUTTON_LABEL,
        'Height': BUTTON_IMAGE,
        'ImageURL': 'report.png',
        'FocusOnClick': False,
        'ImagePosition': 1,
    }
    dialog.add_control(args)

    dialog.center(dialog.lbl_source_dir, y=5)
    dialog.txt_source_dir.move(dialog.lbl_source_dir, y=9)
    dialog.cmd_source_dir.move(dialog.lbl_source_dir)

    dialog.lbl_source_template.move(dialog.cmd_source_dir)
    dialog.txt_source_template.move(dialog.lbl_source_template, y=8)
    dialog.cmd_source_template.move(dialog.lbl_source_template)

    dialog.cmd_report.move(dialog.cmd_source_template)
    dialog.lbl_source_template.center()
    dialog.cmd_report.center()

    controls = (dialog.txt_source_dir, dialog.cmd_source_dir)
    dialog.center(controls)
    controls = (dialog.txt_source_template, dialog.cmd_source_template)
    dialog.center(controls)

    values = app.get_config('report', (), APP)
    if values:
        dialog.txt_source_dir.value = values[0]
        dialog.txt_source_template.value = values[1]

    return dialog
