import uno
import unohelper
from com.sun.star.task import XJobExecutor
from easymacro import catch_exception

import main


ID_EXTENSION = 'net.elmau.zaz.cfdi'
SERVICE = ('com.sun.star.task.Job',)


class ZazCfdi(unohelper.Base, XJobExecutor):

    def __init__(self, ctx):
        self.ctx = ctx

    @catch_exception
    def trigger(self, args):
        main.ID_EXTENSION = ID_EXTENSION
        main.run(args)
        return


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(ZazCfdi, ID_EXTENSION, SERVICE)
